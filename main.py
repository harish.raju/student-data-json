from scripts.core.studentinfo import StudentInfo

obj = StudentInfo()
if __name__ == '__main__':
    info = dict()
    info["Name"] = input("Enter name:")
    info["Class"] = input("Enter class:")
    info["Department"] = input("Enter department:")
    info["College"] = input("Enter college:")
    lst = []
    courses = int(input("Enter no. of courses to enroll:"))
    for i in range(courses):
        crses = dict()
        crses["Course_Code" + str(i + 1)] = input("Enter course code" + str(i + 1) + ":")
        crses["Course_Name" + str(i + 1)] = input("Enter course name" + str(i + 1) + ":")
        lst.append(crses)
    info["Courses"] = lst
    print(obj.dis(info))
